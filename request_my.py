from aiogram.dispatcher.storage import FSMContext
from aiogram.types.input_media import InputMediaAnimation
from aiogram.dispatcher.filters import Command
from aiogram.types.message import ContentType
from aiogram.types.reply_keyboard import ReplyKeyboardRemove
from database import add_exp, add_request, add_user, check_request_1, del_user, edit_request, get_deferred_requests, get_exp, get_exps, get_request, get_requests, get_requests_by_status, get_requests_by_status_other, get_user, get_users, my_request_stats
from keyboards import kb_admin, kb_back, kb_confirm, kb_edit, kb_exp, kb_pay_method, kb_request, kb_request_cashless, kb_request_status, kb_request_status_next, kb_requests, kb_send, kb_skip, kb_start, kb_users
from load_bot import dp
from aiogram import types
from messages import MESSAGES
from states import Admin, MainMenu, MyRequests, NewRequest, Request
from datetime import datetime
from variables import TEXT


@dp.callback_query_handler(lambda c: c.data == 'back', state=MainMenu.my_requests)
async def back_my_requests(call: types.CallbackQuery, state: FSMContext):
    db = await check_request_1(call.from_user.id)
    kb = await kb_request_status()
    ans = '<u>Мои запросы</u>\n'
    if db[0] > 0:
        ans += f'❗️<b>Есть неподтвержденные запросы: <u>{db[0]}</u> на сумму <i>{db[1]} руб.</i></b>'
    else:
        ans += 'У вас нет неподтвержденных запросов'
    await call.message.edit_text(ans, reply_markup=kb, parse_mode='HTML')
    await MainMenu.my_requests.set()


@dp.callback_query_handler(lambda c: c.data == 'back', state=MyRequests.stats)
@dp.callback_query_handler(lambda c: c.data == 'my_stats', state=MainMenu.my_requests)
async def back_my_requests(call: types.CallbackQuery, state: FSMContext):
    db = await my_request_stats(call.from_user.id)
    stats = 'Всего запросов: <b>{}</b> ➖<i>{} руб.</i>\n' \
            'Оплачен: <b>{}</b> ➖<i>{} руб.</i>\n' \
            'Отклонен: <b>{}</b> ➖<i>{} руб.</i>\n' \
            'На рассмотрении: <b>{}</b> ➖<i>{} руб.</i>\n'.format(
                db[3][0], db[3][1],
                db[1][0], db[1][1],
                db[2][0], db[2][1],
                db[0][0], db[0][1]
            ).replace('None', '0')
    kb = await kb_request_status_next()
    await call.message.edit_text(f'<u>Мои запросы</u>\n{stats}', reply_markup=kb, parse_mode='HTML')
    await MainMenu.my_requests.set()


@dp.callback_query_handler(lambda c: c.data.startswith('request'), state=MainMenu.my_requests)
async def edit_deferred_request(call: types.CallbackQuery):
    db = await get_request(call.data[7:])
    kb = await kb_confirm(db[0])
    msg1 = TEXT['request_sent'].format(db[0], db[2], db[3], '💵', db[4], '💵 Подтвердите оплату')
    if db[5] == 0:
        await call.message.edit_text(msg1, reply_markup=kb, parse_mode='HTML')
    elif db[5] == 1:
        await dp.bot.delete_message(call.message.chat.id, call.message.message_id)
        await call.message.answer_photo(photo=db[6], caption=msg1, reply_markup=kb, parse_mode='HTML')
    elif db[5] == 2:
        await dp.bot.delete_message(call.message.chat.id, call.message.message_id)
        await call.message.answer_document(document=db[6], caption=msg1, reply_markup=kb, parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data == 'confirm_all', state=MainMenu.my_requests)
async def my_requests_cash(call: types.CallbackQuery, state: FSMContext):

    uid = call.from_user.id
    status = 1
    db_confirm = await get_requests_by_status(uid, status)

    if db_confirm:
        for db in db_confirm:
            db = await edit_request(db[0], 2, 'Оплачен', None)
            msg2 = TEXT['request_to'].format(db[0], db[8], db[9], db[2], db[3], '💵', db[1], db[4], '✅ Оплачен')
            if db[5] == 0:
                await dp.bot.send_message(chat_id=db[10], text=msg2, parse_mode='HTML')
            elif db[5] == 1:
                await dp.bot.send_photo(chat_id=db[10], photo=db[6], caption=msg2, parse_mode='HTML')
            elif db[5] == 2:
                await dp.bot.send_document(chat_id=db[10], document=db[6], caption=msg2, parse_mode='HTML')
        await dp.bot.answer_callback_query(callback_query_id=call.id,
                        text='Успешно! Все запросы подтверждены', show_alert=True)
    else:
        await dp.bot.answer_callback_query(callback_query_id=call.id,
                        text='Нет запросов для подтверждения', show_alert=True)


@dp.callback_query_handler(lambda c: c.data.startswith('my_req_'), state=MainMenu.my_requests)
async def my_requests_cash(call: types.CallbackQuery, state: FSMContext):

    uid = call.from_user.id
    status = int(call.data[7:])
    db = await get_requests_by_status(uid, status)

    if db:
        ans = f'<u>Мои запросы</u> -> <b>{db[0][13]}</b>\n'
        for i in db:
            if i[1] == 'Наличные':
                pay_method = '💵'
            else:
                pay_method = '💳'
            ans += TEXT['my_stats'].format(i[0], i[2], i[3], pay_method)
        if status == 1:
            kb = await kb_requests(db)
        else:
            kb = await kb_back()
        await call.message.edit_text(ans, reply_markup=kb, parse_mode='HTML')
    else:
        kb = await kb_back()
        await call.message.edit_text('Запросов нет', reply_markup=kb, parse_mode='HTML')
    if status != 1:
        await MyRequests.stats.set()
