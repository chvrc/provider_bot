from aiogram.dispatcher.filters.builtin import CommandStart
from aiogram.dispatcher.storage import FSMContext
from aiogram.types.input_media import InputMediaAnimation
from aiogram.dispatcher.filters import Command
from aiogram.types.message import ContentType
from aiogram.types.reply_keyboard import ReplyKeyboardRemove
from database import add_exp, add_request, add_user, check_deferred_requests, check_my_requests, check_request_0, check_request_1, check_user, deferred_request_stats, del_user, edit_request, get_all_requests, get_deferred_requests, get_exp, get_exps, get_request, get_request_by_id, get_requests, get_user, get_users, my_request_stats
from keyboards import kb_admin, kb_back, kb_confirm, kb_edit, kb_exp, kb_pay_method, kb_request, kb_request_deferred, kb_request_status, kb_send, kb_skip, kb_start, kb_users
from load_bot import ADMIN_ID, INVITE_HASH, SHEET_ID, dp, bot
from aiogram import types
from messages import MESSAGES
from states import Admin, MainMenu, NewRequest, NewUser, Request
from datetime import datetime
import aioschedule
import asyncio

from variables import TEXT, append_google_sheet


@dp.message_handler(lambda message: check_user(message.from_user.id), Command('check'), state='*')
async def new_request(message: types.Message):
    await message.answer("<b>Внимание!</b>\n\nПрисоединиться к боту можно \
<b>только по ссылке-приглашению</b> или если тебя добавят в систему вручную.\n\
Спроси эту ссылку у администратора", parse_mode='HTML')


@dp.message_handler(lambda message: message.get_args() == INVITE_HASH, CommandStart(), state='*')
async def new_request(message: types.Message):
    await message.answer('Добро пожаловать!\nЧтобы завершить регистрацию введите ваше ФИО:')
    await NewUser.start.set()


@dp.message_handler(state=NewUser.start)
async def new_request(message: types.Message, state: FSMContext):
    await add_user(message.from_user.id, message.text)
    kb = await kb_start(message.from_user.id)
    await message.answer('Регистрация прошла успешно!', reply_markup=kb)
    await state.finish()


@dp.message_handler(CommandStart(), state='*')
async def new_request(message: types.Message):
    await message.answer("<b>Внимание!</b>\n\nПрисоединиться к боту можно \
<b>только по ссылке-приглашению</b> или если тебя добавят в систему вручную.\n\
Спроси эту ссылку у администратора", parse_mode='HTML')


@dp.message_handler(lambda message: message.text.startswith("/N_"), state='*', user_id=ADMIN_ID)
async def new_request_back(message: types.Message, state: FSMContext):

    req_id = message.text[3:]

    db = await get_request(req_id)
    if db:
        if db[1] == 'Наличные':
            ans = TEXT['request_new'].format(db[2], db[3], '💵', db[4])
        else:
            ans = TEXT['request_new'].format(db[2], db[3], '💳', db[4])
        if db[5] == 0:
            await message.answer(ans, parse_mode='HTML')
        elif db[5] == 1:
            await message.answer_photo(photo=db[6], caption=ans, parse_mode='HTML')
        elif db[5] == 2:
            await message.answer_document(document=db[6], caption=ans, parse_mode='HTML')
    else:
        await message.answer('Такого запроса нет')


@dp.message_handler(lambda message: message.text.startswith("/N_"), state='*')
async def new_request_back(message: types.Message, state: FSMContext):

    uid = message.from_user.id
    req_id = message.text[3:]

    db = await get_request_by_id(uid, req_id)
    if db:
        if db[1] == 'Наличные':
            ans = TEXT['request_new'].format(db[2], db[3], '💵', db[4])
        else:
            ans = TEXT['request_new'].format(db[2], db[3], '💳', db[4])
        if db[5] == 0:
            await message.answer(ans, parse_mode='HTML')
        elif db[5] == 1:
            await message.answer_photo(photo=db[6], caption=ans, parse_mode='HTML')
        elif db[5] == 2:
            await message.answer_document(document=db[6], caption=ans, parse_mode='HTML')
    else:
        await message.answer('Такого запроса нет')
    


@dp.message_handler(lambda message: check_user(message.from_user.id), Command('new'), state='*')
@dp.message_handler(lambda message: message.text == TEXT['new_request'], state='*')
async def new_request_back(message: types.Message, state: FSMContext):
    db = await get_user(message.from_user.id)
    if db:
        async with state.proxy() as data:
            data['from_uid'] = db[0][1]
            data['from_name'] = db[0][2]
        kb = await kb_pay_method()
        await message.answer('Выберите способ оплаты', reply_markup=kb)
        await NewRequest.pay_method.set()
    else:
        await state.finish()
        await message.answer('Вас нет в базе пользователей. Обратитесь к администратору.')


@dp.message_handler(lambda message: message.text == TEXT['my_requests'], state='*')
async def new_request_back(message: types.Message, state: FSMContext):
    db = await check_request_1(message.from_user.id)
    kb = await kb_request_status()
    ans = '<u>Мои запросы</u>\n'
    if db[0] > 0:
        ans += f'❗️<b>У вас <u>{db[0]}</u> неподтвержденных запросов на сумму <i>{db[1]} руб.</i></b>'
    else:
        ans += 'У вас нет неподтвержденных запросов'
    await message.answer(ans, reply_markup=kb, parse_mode='HTML')
    await MainMenu.my_requests.set()


@dp.message_handler(lambda message: message.text == TEXT['deferred_requests'], state='*')
async def new_request_back(message: types.Message, state: FSMContext):
    db = await check_request_0(message.from_user.id)
    ans = '<u>Полученные запросы</u>\n'
    if db[0] > 0:
        ans += f'❗️<b>У вас <u>{db[0]}</u> неоплаченных запросов на сумму <i>{db[1]} руб.</i></b>'
    else:
        ans += 'У вас нет неоплаченных запросов'
    kb = await kb_request_deferred()
    await message.answer(ans, reply_markup=kb, parse_mode='HTML')
    await MainMenu.deferred_requests.set()


@dp.message_handler(Command('admin'), state='*', user_id=ADMIN_ID)
@dp.message_handler(lambda message: message.text == TEXT['admin_panel'], state='*', user_id=ADMIN_ID)
async def admin_start(message: types.Message):
    kb = await kb_admin()
    await message.answer('Админ-панель', reply_markup=kb)
    await Admin.start.set()


@dp.message_handler(Command('menu'), state='*')
async def new_request(message: types.Message, state: FSMContext):
    await state.finish()
    db = await get_user(message.from_user.id)
    if db:
        kb = await kb_start(message.from_user.id)
        await message.answer('Главное меню', reply_markup=kb)
    else:
        await message.answer('Вас нет в базе пользователей. Обратитесь к администратору.')


async def noon_print():
    arr = await get_all_requests()
    await append_google_sheet(arr, SHEET_ID)


async def scheduler():
    aioschedule.every().day.at("07:00").do(noon_print)
    while True:
        await aioschedule.run_pending()
        await asyncio.sleep(1)