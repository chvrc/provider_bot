from menu import scheduler
from set_bot_commands import set_default_commands
from aiogram.utils import executor
from load_bot import bot, loop
from database import create_db
import asyncio


async def on_shutdown(dp):
    await bot.close()


async def on_startup(dispatcher):
    await set_default_commands(dispatcher)
    await create_db()
    asyncio.create_task(scheduler())


if __name__ == "__main__":
    from menu import dp
    from admin import dp
    from request import dp
    from request_new import dp
    from request_my import dp
    from request_deferred import dp
    executor.start_polling(dp, loop=loop,on_shutdown=on_shutdown, on_startup=on_startup)
