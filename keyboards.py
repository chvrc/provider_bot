from aiogram.types import (InlineKeyboardMarkup, InlineKeyboardButton, 
                            ReplyKeyboardMarkup, KeyboardButton)
import time
from database import get_exp_user, get_exps, get_users
from load_bot import ADMIN_ID, SHEET_ID, bot
from variables import TEXT


# async def kb_start():
#     bt1 = InlineKeyboardButton('Создать запрос', callback_data='new_request')
#     bt2 = InlineKeyboardButton('Мои запросы', callback_data='my_requests')
#     bt3 = InlineKeyboardButton('Отложенные запросы', callback_data='deferred_requests')
#     kb = InlineKeyboardMarkup()
#     kb.row(bt1, bt2).add(bt3)
#     return kb


async def kb_start(id):
    kb = ReplyKeyboardMarkup(resize_keyboard=True)
    bt1 = KeyboardButton(TEXT['new_request'])
    bt2 = KeyboardButton(TEXT['my_requests'])
    kb.row(bt1, bt2)
    user = get_exp_user()
    if id in user:
        bt3 = KeyboardButton(TEXT['deferred_requests'])
        kb.add(bt3)
    if id in ADMIN_ID:
        bt4 = KeyboardButton(TEXT['admin_panel'])
        kb.add(bt4)
    return kb


async def kb_admin():
    bt1 = InlineKeyboardButton('👥 Пользователи', callback_data='admin_users')
    bt2 = InlineKeyboardButton('💼 Статьи расходов', callback_data='admin_exp')
    bt3 = InlineKeyboardButton('🔗 Ссылка-приглашение', callback_data='admin_invite_link')
    bt4 = InlineKeyboardButton('📈 Статистика', callback_data='admin_stats')
    kb = InlineKeyboardMarkup()
    kb.row(bt1, bt2).row(bt3, bt4)
    return kb


async def kb_stats():
    bt1 = InlineKeyboardButton('Обновить Google таблицу', callback_data='update_stats')
    bt2 = InlineKeyboardButton('Общая статистика', url=f'https://docs.google.com/spreadsheets/d/{SHEET_ID}/')
    bt3 = InlineKeyboardButton('По способу оплаты', callback_data='stats_by_payment_method')
    bt4 = InlineKeyboardButton('По статье расходов', callback_data='stats_by_purpose')
    bt0 = InlineKeyboardButton('⬅️ Назад', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.row(bt1, bt2).row(bt3, bt4).row(bt0)
    return kb


async def kb_stats_if():
    bt2 = InlineKeyboardButton('Посмотреть статистику', url='https://docs.google.com/spreadsheets/d/1WzUQ6MNMt0NeUi-FakK8tTEI8duoJ7ZYFXN160kJBJM/')
    bt0 = InlineKeyboardButton('⬅️ Назад', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.row(bt2).row(bt0)
    return kb


async def kb_edit():
    bt1 = InlineKeyboardButton('➕ Добавить', callback_data='add')
    bt2 = InlineKeyboardButton('✏️ Изменить', callback_data='edit')
    bt3 = InlineKeyboardButton('➖ Удалить', callback_data='del')
    bt4 = InlineKeyboardButton('⬅️ Назад', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.row(bt1, bt2, bt3).add(bt4)
    return kb


async def kb_users():
    db = await get_users()
    kb = InlineKeyboardMarkup()
    for i in db:
        bt = InlineKeyboardButton(f'{i[2]}', callback_data=f'user{i[0]}')
        kb.add(bt)
    bt0 = InlineKeyboardButton('⬅️ Назад', callback_data='back')
    kb.add(bt0)
    return kb


async def kb_exp():
    db = await get_exps()
    kb = InlineKeyboardMarkup()
    for i in db:
        bt = InlineKeyboardButton(f'{i[1]}', callback_data=f'exp{i[0]}')
        kb.add(bt)
    bt0 = InlineKeyboardButton('⬅️ Назад', callback_data='back')
    kb.add(bt0)
    return kb


async def kb_requests(db):
    kb = InlineKeyboardMarkup()
    for i in db:
        bt = InlineKeyboardButton(f'Запрос №{i[0]}', callback_data=f'request{i[0]}')
        kb.add(bt)
    bt0 = InlineKeyboardButton('⬅️ Назад', callback_data='back')
    kb.add(bt0)
    return kb


async def kb_request(id):
    bt1 = InlineKeyboardButton('💸 Оплатить', callback_data=f'payout{id}')
    bt2 = InlineKeyboardButton('📌 Оставить в запросах', callback_data=f'leave{id}')
    bt3 = InlineKeyboardButton('❌ Отклонить', callback_data=f'reject{id}')
    kb = InlineKeyboardMarkup()
    kb.add(bt2).row(bt1, bt3)
    return kb


async def kb_request_cashless(id):
    bt1 = InlineKeyboardButton('✅ Принять', callback_data=f'accept{id}')
    bt2 = InlineKeyboardButton('📌 Оставить в запросах', callback_data=f'leave{id}')
    bt3 = InlineKeyboardButton('❌ Отклонить', callback_data=f'reject{id}')
    kb = InlineKeyboardMarkup()
    kb.add(bt2).row(bt1, bt3)
    return kb


async def kb_pay_method():
    bt1 = InlineKeyboardButton('💵 Наличные', callback_data='cash')
    bt2 = InlineKeyboardButton('💳 Безналичный', callback_data='cashless')
    kb = InlineKeyboardMarkup()
    kb.add(bt1, bt2)
    return kb


async def kb_request_deferred():
    kb = InlineKeyboardMarkup()
    bt1 = InlineKeyboardButton('Статистика', callback_data='def_stats')
    bt5 = InlineKeyboardButton('❗️Неоплаченные', callback_data='def_req_0')
    bt6 = InlineKeyboardButton('💵 Оплатить всё', callback_data='pay_all')
    kb.row(bt1).add(bt5).add(bt6)
    return kb


async def kb_request_deferred_next():
    kb = InlineKeyboardMarkup()
    bt1 = InlineKeyboardButton('✅ Оплаченные', callback_data='def_req_2')
    bt2 = InlineKeyboardButton('❌ Отклоненные', callback_data='def_req_3')
    bt4 = InlineKeyboardButton('❔ Ожидают подтверждения', callback_data='def_req_1')
    bt5 = InlineKeyboardButton('Назад', callback_data='back')
    kb.row(bt1, bt2).add(bt4).add(bt5)
    return kb


async def kb_request_status():
    kb = InlineKeyboardMarkup()
    bt1 = InlineKeyboardButton('Статистика', callback_data='my_stats')
    bt4 = InlineKeyboardButton('❗️Неподтвержденные', callback_data='my_req_1')
    bt5 = InlineKeyboardButton('💵 Подтвердить всё', callback_data='confirm_all')
    kb.row(bt1).add(bt4).add(bt5)
    return kb


async def kb_request_status_next():
    kb = InlineKeyboardMarkup()
    bt1 = InlineKeyboardButton('✅ Оплаченные', callback_data='my_req_2')
    bt2 = InlineKeyboardButton('❌ Отклоненные', callback_data='my_req_3')
    bt3 = InlineKeyboardButton('❔ На рассмотрении', callback_data='my_req_0')
    bt4 = InlineKeyboardButton('Назад', callback_data='back')
    kb.row(bt1, bt2).add(bt3).add(bt4)
    return kb


async def kb_confirm(id):
    bt1 = InlineKeyboardButton('✅ Да', callback_data=f'yes{id}')
    bt2 = InlineKeyboardButton('❌ Нет', callback_data=f'no{id}')
    kb = InlineKeyboardMarkup()
    kb.row(bt1, bt2)
    return kb


async def kb_send():
    bt1 = InlineKeyboardButton('✉️ Отправить', callback_data='send')
    bt2 = InlineKeyboardButton('🗑 Отмена', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.row(bt1, bt2)
    return kb


async def kb_back():
    bt1 = InlineKeyboardButton('⬅️Назад', callback_data='back')
    kb = InlineKeyboardMarkup()
    kb.add(bt1)
    return kb


async def kb_skip():
    bt1 = InlineKeyboardButton('Пропустить', callback_data='skip')
    kb = InlineKeyboardMarkup()
    kb.add(bt1)
    return kb


async def kb_cancel():
    bt1 = KeyboardButton('Отмена')
    kb = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    kb.add(bt1)
    return kb