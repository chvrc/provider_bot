from aiogram.dispatcher.storage import FSMContext
from aiogram.dispatcher.filters import Command
from database import add_exp, add_user, all_stats, all_stats_exp, del_exp, del_user, edit_exp_name, edit_exp_user, edit_user, get_all_requests, get_all_requests_cash, get_all_requests_exp, get_exp_name, get_exps, get_users, request_stats_exp, request_stats_pay
from keyboards import kb_admin, kb_back, kb_edit, kb_exp, kb_pay_method, kb_start, kb_stats, kb_stats_if, kb_users
from load_bot import ADMIN_ID, INVITE_HASH, SHEET_ID, dp
from aiogram import types
from states import Admin
from variables import TEXT, append_google_sheet


@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.users)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.exp)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.inv_link)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.stats)
async def back_admin_start(call: types.CallbackQuery, state: FSMContext):
    kb = await kb_admin()
    await call.message.edit_text('Админ-панель', reply_markup=kb)
    await Admin.start.set()


@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.add_user_id)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.add_user_name)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.edit_user)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.edit_user_start)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.del_user)
@dp.callback_query_handler(lambda c: c.data == 'admin_users', state=Admin.start)
async def admin_users(call: types.CallbackQuery):
    kb = await kb_edit()
    db = await get_users()
    ans = '👥 <u>Пользователи</u>'
    for user in db:
        ans += f'\n➖<a href="tg://user?id={user[1]}">{user[2]}</a>'
    await call.message.edit_text(ans, reply_markup=kb, parse_mode='HTML')
    await Admin.users.set()


@dp.callback_query_handler(lambda c: c.data == 'add', state=Admin.users)
async def admin_add_user(call: types.CallbackQuery):
    kb = await kb_back()
    await call.message.edit_text('Введите имя нового пользователя', reply_markup=kb)
    await Admin.add_user_name.set()


@dp.message_handler(state=Admin.add_user_name)
async def admin_add_user_name(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['user_name'] = message.text
    await message.answer('Введите ID пользователя в Telegram')
    await Admin.add_user_id.set()


@dp.message_handler(lambda message: message.text.isdigit(), state=Admin.add_user_id)
async def admin_add_user_id(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        name = data['user_name']
    db = await add_user(int(message.text), name)
    kb = await kb_back()
    await message.answer(f'Добавлен новый пользователь:\n<a href="tg://user?id={db[0]}">{db[1]}</a>',
                    reply_markup=kb, parse_mode='HTML')


@dp.message_handler(state=Admin.add_user_id)
async def admin_add_user_id(message: types.Message, state: FSMContext):
    await message.answer('Неверный формат! Введите ID пользователя (целое число)',
                    parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data == 'edit', state=Admin.users)
async def admin_del_user(call: types.CallbackQuery):
    kb = await kb_users()
    await call.message.edit_text('Выберите пользователя для изменения имени', reply_markup=kb)
    await Admin.edit_user_start.set()


@dp.callback_query_handler(lambda c: c.data.startswith('user'), state=Admin.edit_user_start)
async def admin_del_user_id(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        data['user_id'] = call.data[4:]
    await call.message.edit_text('Введите новое имя пользователя', parse_mode='HTML')
    await Admin.edit_user.set()


@dp.message_handler(state=Admin.edit_user)
async def admin_add_user_name(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        user_id = data['user_id']
    await edit_user(user_id, message.text)
    kb = await kb_back()
    await message.answer(f'Имя пользователя изменено на <b>{message.text}</b>', 
                    reply_markup=kb, parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data == 'del', state=Admin.users)
async def admin_del_user(call: types.CallbackQuery):
    kb = await kb_users()
    await call.message.edit_text('Выберите пользователя для удаления', reply_markup=kb)
    await Admin.del_user.set()


@dp.callback_query_handler(lambda c: c.data.startswith('user'), state=Admin.del_user)
async def admin_del_user_id(call: types.CallbackQuery):
    db = await del_user(call.data[4:])
    kb = await kb_back()
    await call.message.edit_text(f'Пользователь <a href="tg://user?id={db[1]}">{db[2]}</a> удалён',
                            reply_markup=kb, parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.add_exp)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.add_exp_user)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.edit_exp)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.edit_exp_start)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.del_exp)
@dp.callback_query_handler(lambda c: c.data == 'admin_exp', state=Admin.start)
async def admin_exp(call: types.CallbackQuery):
    kb = await kb_edit()
    db = await get_exps()
    ans = '💼 <u>Статьи расходов</u>'
    for i in db:
        ans += f'\n➖<b>{i[1]}</b> 👤<i><a href="tg://user?id={i[2]}">{i[3]}</a></i>'
    await call.message.edit_text(ans, reply_markup=kb, parse_mode='HTML')
    await Admin.exp.set()


@dp.callback_query_handler(lambda c: c.data == 'add', state=Admin.exp)
async def admin_add_exp(call: types.CallbackQuery):
    kb = await kb_back()
    await call.message.edit_text('Введите название статьи расхода', reply_markup=kb)
    await Admin.add_exp.set()


@dp.message_handler(state=Admin.add_exp)
async def admin_add_exp_name(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['exp_name'] = message.text
    kb = await kb_users()
    await message.answer('Выберите ответственного:', reply_markup=kb)
    await Admin.add_exp_user.set()


@dp.callback_query_handler(lambda c: c.data.startswith('user'), state=Admin.add_exp_user)
async def admin_add_exp_user(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        name = data['exp_name']
    db = await add_exp(name, call.data[4:])
    kb = await kb_back()
    await call.message.edit_text(f'Добавлена статья расхода: <b>{db[2]}</b> 👤<i><a href="tg://user?id={db[0]}">{db[1]}</a></i>',
                                    reply_markup=kb, parse_mode='HTML')
    kb2 = await kb_start(db[0])
    await dp.bot.send_message(chat_id=db[0], text=f'Вы назначены ответственным за статью расходов: <b>{db[2]}</b>',
                        reply_markup=kb2, parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data == 'edit', state=Admin.exp)
async def admin_del_user(call: types.CallbackQuery):
    kb = await kb_exp()
    await call.message.edit_text('Выберите статью для редактирования', reply_markup=kb)
    await Admin.edit_exp_start.set()


@dp.callback_query_handler(lambda c: c.data.startswith('exp'), state=Admin.edit_exp_start)
async def admin_del_user_id(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        data['exp_id'] = call.data[3:]
    kb = await kb_users()
    await call.message.edit_text('Введите новое название или выберите нового ответственного',
                reply_markup=kb, parse_mode='HTML')
    await Admin.edit_exp.set()


@dp.message_handler(state=Admin.edit_exp)
async def admin_add_exp_name(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        exp_id = data['exp_id']
    db = await edit_exp_name(exp_id, message.text)
    kb = await kb_back()
    await message.answer(f'Отредактирована статья расхода: <b>{db[2]}</b> 👤<i><a href="tg://user?id={db[0]}">{db[1]}</a></i>',
                                    reply_markup=kb, parse_mode='HTML')
    kb2 = await kb_start(db[0])
    await dp.bot.send_message(chat_id=db[0], text=f'Вы назначены ответственным за статью расходов: <b>{db[2]}</b>',
                        reply_markup=kb2, parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data.startswith('user'), state=Admin.edit_exp)
async def admin_add_exp_user(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        exp_id = data['exp_id']
    db = await edit_exp_user(exp_id, call.data[4:])
    kb = await kb_back()
    await call.message.edit_text(f'Отредактирована статья расхода: <b>{db[2]}</b> 👤<i><a href="tg://user?id={db[0]}">{db[1]}</a></i>',
                                    reply_markup=kb, parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data == 'del', state=Admin.exp)
async def admin_del_user(call: types.CallbackQuery):
    kb = await kb_exp()
    await call.message.edit_text('Выберите статью для удаления', reply_markup=kb)
    await Admin.del_exp.set()


@dp.callback_query_handler(lambda c: c.data.startswith('exp'), state=Admin.del_exp)
async def admin_del_user_id(call: types.CallbackQuery):
    await del_exp(call.data[3:])
    kb = await kb_back()
    await call.message.edit_text('Статья удалена', reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'admin_invite_link', state=Admin.start)
async def admin_invite_link(call: types.CallbackQuery):
    kb = await kb_back()
    link = await dp.bot.get_me()
    await call.message.edit_text(f"🔗 <u>Ссылка-приглашение</u>\nДля подключения своих сотрудников к боту, отправь им ссылку https://t.me/{link.username}?start={INVITE_HASH}",
                            reply_markup=kb, parse_mode='HTML')
    await Admin.inv_link.set()


""""Статистика"""


@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.stats_cash)
@dp.callback_query_handler(lambda c: c.data == 'back', state=Admin.stats_exp)
@dp.callback_query_handler(lambda c: c.data == 'admin_stats', state=Admin.start)
async def admin_invite_link(call: types.CallbackQuery):
    db = await all_stats()
    kb = await kb_stats()
    stats_exp = ''
    db_exps = await all_stats_exp()
    for i in db_exps:
        stats_exp += '➕{}: <b>{}</b> ➖ <i>{} руб.</i>\n'.format(i[0], i[1], i[2])
    await call.message.edit_text('📈 <u>Статистика</u>\n' \
        'Всего запросов: <b>{}</b> ➖ <i>{} руб.</i>\n' \
        'Оплачено: <b>{}</b> ➖ <i>{} руб.</i>\n' \
        '{}\n' \
        '💲Наличные: <b>{}</b> ➖ <i>{} руб.</i>\n' \
        '💲Безналичные: <b>{}</b> ➖ <i>{} руб.</i>\n'.format(
                        db[0][0], db[0][1],
                        db[1][0], db[1][1], stats_exp,
                        db[2][0], db[2][1],
                        db[3][0], db[3][1]).replace('None', '0'),
                        reply_markup=kb, parse_mode='HTML')
    await Admin.stats.set()


@dp.callback_query_handler(lambda c: c.data == 'update_stats', state=Admin.stats)
async def admin_invite_link(call: types.CallbackQuery):
    arr = await get_all_requests()
    await append_google_sheet(arr, SHEET_ID)
    await dp.bot.answer_callback_query(callback_query_id=call.id,
                        text='Обновлено', show_alert=True)


@dp.callback_query_handler(lambda c: c.data == 'stats_by_payment_method', state=Admin.stats)
async def admin_invite_link(call: types.CallbackQuery):
    kb = await kb_pay_method()
    await call.message.edit_text('Выберите метод оплаты', reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data == 'stats_by_purpose', state=Admin.stats)
async def admin_invite_link(call: types.CallbackQuery):
    kb = await kb_exp()
    await call.message.edit_text('Выберите статью расходов', reply_markup=kb)


@dp.callback_query_handler(lambda c: c.data.startswith('cash'), state=Admin.stats)
async def stats_by_payment_method(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        data['stats_payment_method'] = call.data
    await call.message.edit_text('Введите количество дней для вывода статистики (целое число)')
    await Admin.stats_cash.set()


@dp.callback_query_handler(lambda c: c.data.startswith('exp'), state=Admin.stats)
async def stats_by_purpose(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        data['stats_purpose'] = call.data[3:]
    await call.message.edit_text('Введите количество дней для вывода статистики (целое число)')
    await Admin.stats_exp.set()


@dp.message_handler(lambda message: message.text.isdigit(), state=Admin.stats_cash)
async def admin_add_exp_name(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        if data['stats_payment_method'] == 'cash':
            pay_method = 'Наличные'
        elif data['stats_payment_method'] == 'cashless':
            pay_method = 'Безналичные'
    db = await request_stats_pay(pay_method, int(message.text))
    stats = 'Всего запросов: <b>{}</b> ➖<i>{} руб.</i>\n' \
            'Оплачен: <b>{}</b> ➖<i>{} руб.</i>\n' \
            'Отклонен: <b>{}</b> ➖<i>{} руб.</i>\n' \
            'На рассмотрении: <b>{}</b> ➖<i>{} руб.</i>\n' \
            'Ожидают подтверждения: <b>{}</b> ➖<i>{} руб.</i>'.format(
                db[4][0], db[4][1],
                db[2][0], db[2][1],
                db[3][0], db[3][1],
                db[0][0], db[0][1],
                db[1][0], db[1][1],
            ).replace('None', '0')
    kb = await kb_back()
    await message.answer(f'<u>Статистика</u> -> {pay_method} -> {message.text} д\n{stats}',
                        reply_markup=kb, parse_mode='HTML')


@dp.message_handler(lambda message: message.text.isdigit(), state=Admin.stats_exp)
async def admin_add_exp_name(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        stats_purpose = data['stats_purpose']
    purpose = await get_exp_name(stats_purpose)
    db = await request_stats_exp(purpose, int(message.text))
    stats = 'Всего запросов: <b>{}</b> ➖<i>{} руб.</i>\n' \
            'Оплачен: <b>{}</b> ➖<i>{} руб.</i>\n' \
            'Отклонен: <b>{}</b> ➖<i>{} руб.</i>\n' \
            'На рассмотрении: <b>{}</b> ➖<i>{} руб.</i>\n' \
            'Ожидают подтверждения: <b>{}</b> ➖<i>{} руб.</i>'.format(
                db[4][0], db[4][1],
                db[2][0], db[2][1],
                db[3][0], db[3][1],
                db[0][0], db[0][1],
                db[1][0], db[1][1],
            ).replace('None', '0')
    kb = await kb_back()
    await message.answer(f'<u>Статистика</u> -> {purpose} -> {message.text} д\n{stats}',
                        reply_markup=kb, parse_mode='HTML')