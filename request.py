from aiogram.dispatcher.storage import FSMContext
from database import edit_request, get_request
from keyboards import kb_confirm, kb_request, kb_request_cashless
from load_bot import dp
from aiogram import types
from states import MainMenu, Request
from variables import TEXT


@dp.callback_query_handler(lambda c: c.data.startswith('payout'), state='*')
async def accept_request(call: types.CallbackQuery):
    db = await edit_request(call.data[6:], 1, 'Ожидание подтверждения оплаты', None)
    kb = await kb_confirm(db[0])
    if db[1] == 'Наличные':
        pay_method = '💵'
    else:
        pay_method = '💳'
    msg1 = TEXT['request_sent'].format(db[0], db[2], db[3], pay_method, db[4], '❕ Подтвердите оплату')
    msg2 = TEXT['request_to'].format(db[0], db[8], db[9], db[2], db[3], pay_method, db[4], '❕ Ожидание подтверждения')
    if db[5] == 0:
        await dp.bot.send_message(chat_id=db[8], text=msg1, reply_markup=kb, parse_mode='HTML')
        await call.message.edit_text(msg2, parse_mode='HTML')
    elif db[5] == 1:
        await dp.bot.send_photo(chat_id=db[8], photo=db[6], caption=msg1, reply_markup=kb, parse_mode='HTML')
        await call.message.edit_caption(msg2, parse_mode='HTML')
    elif db[5] == 2:
        await dp.bot.send_document(chat_id=db[8], document=db[6], caption=msg1, reply_markup=kb, parse_mode='HTML')
        await call.message.edit_caption(msg2, parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data.startswith('leave'), state='*')
async def leave_request(call: types.CallbackQuery):
    db = await edit_request(call.data[5:], 0, 'На рассмотрении', None)
    if db[1] == 'Наличные':
        pay_method = '💵'
    else:
        pay_method = '💳'
    msg = TEXT['request_to'].format(db[0], db[8], db[9], db[2], db[3], pay_method, db[4], '⏳ Отложен')
    if db[5] == 0:
        await call.message.edit_text(msg, parse_mode='HTML')
    else:
        await call.message.edit_caption(msg, parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data.startswith('reject'), state='*')
async def reject_request(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        data['request_id'] = call.data[6:]
    await call.message.edit_reply_markup()
    await call.message.answer('Введите причину отказа')
    await Request.reject.set()


@dp.message_handler(state=Request.reject)
async def reject_reason(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        request_id = data['request_id']
    db = await edit_request(request_id, 3, 'Отклонен', message.text)
    if db[1] == 'Наличные':
        pay_method = '💵'
    else:
        pay_method = '💳'
    msg1 = TEXT['request_sent'].format(db[0], db[2], db[3], pay_method, db[4], f'❌ Запрос отклонен по причине: {message.text}')
    msg2 = TEXT['request_to'].format(db[0], db[8], db[9], db[2], db[3], pay_method, db[4], '❌ Запрос отклонен')
    if db[5] == 0:
        await dp.bot.send_message(chat_id=db[8], text=msg1, parse_mode='HTML')
        await message.answer(msg2, parse_mode='HTML')
    elif db[5] == 1:
        await dp.bot.send_photo(chat_id=db[8], photo=db[6], caption=msg1, parse_mode='HTML')
        await message.answer_photo(photo=db[6], caption=msg2, parse_mode='HTML')
    elif db[5] == 2:
        await dp.bot.send_document(chat_id=db[8], document=db[6], caption=msg1, parse_mode='HTML')
        await message.answer_document(document=db[6], caption=msg2, parse_mode='HTML')
    await state.finish()


@dp.callback_query_handler(lambda c: c.data.startswith('yes'), state='*')
async def accept_request(call: types.CallbackQuery):
    db = await edit_request(call.data[3:], 2, 'Оплачен', None)
    if db[1] == 'Наличные':
        pay_method = '💵'
    else:
        pay_method = '💳'
    msg1 = TEXT['request_sent'].format(db[0], db[2], db[3], pay_method, db[4], '✅ Оплачен')
    msg2 = TEXT['request_to'].format(db[0], db[8], db[9], db[2], db[3], pay_method, db[4], '✅ Оплачен')
    if db[5] == 0:
        await dp.bot.send_message(chat_id=db[10], text=msg2, parse_mode='HTML')
        await call.message.edit_text(msg1, parse_mode='HTML')
    elif db[5] == 1:
        await dp.bot.send_photo(chat_id=db[10], photo=db[6], caption=msg2, parse_mode='HTML')
        await call.message.edit_caption(msg1, parse_mode='HTML')
    elif db[5] == 2:
        await dp.bot.send_document(chat_id=db[10], document=db[6], caption=msg2, parse_mode='HTML')
        await call.message.edit_caption(msg1, parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data.startswith('no'), state='*')
async def accept_request(call: types.CallbackQuery):
    db = await edit_request(call.data[2:], 0, 'На рассмотрении', None)
    if db[1] == 'Наличные':
        pay_method = '💵'
    else:
        pay_method = '💳'
    msg1 = TEXT['request_sent'].format(db[0], db[2], db[3], pay_method, db[4], '🚫 Платеж не прошел')
    msg2 = TEXT['request_to'].format(db[0], db[8], db[9], db[2], db[3], pay_method, db[4], '🚫 Платеж не прошел')
    if db[5] == 0:
        await dp.bot.send_message(chat_id=db[10], text=msg2, parse_mode='HTML')
        await call.message.edit_text(msg1, parse_mode='HTML')
    elif db[5] == 1:
        await dp.bot.send_photo(chat_id=db[10], photo=db[6], caption=msg2, parse_mode='HTML')
        await call.message.edit_caption(msg1, parse_mode='HTML')
    elif db[5] == 2:
        await dp.bot.send_document(chat_id=db[10], document=db[6], caption=msg2, parse_mode='HTML')
        await call.message.edit_caption(msg1, parse_mode='HTML')


@dp.callback_query_handler(lambda c: c.data.startswith('accept'), state='*')
async def accept_request(call: types.CallbackQuery):
    db = await edit_request(call.data[6:], 2, 'Оплачен', None)
    if db[1] == 'Наличные':
        pay_method = '💵'
    else:
        pay_method = '💳'
    msg1 = TEXT['request_sent'].format(db[0], db[2], db[3], pay_method, db[4], '✅ Оплачен')
    msg2 = TEXT['request_to'].format(db[0], db[8], db[9], db[2], db[3], pay_method, db[4], '✅ Оплачен')
    if db[5] == 0:
        await dp.bot.send_message(chat_id=db[8], text=msg1, parse_mode='HTML')
        await call.message.edit_text(msg2, parse_mode='HTML')
    elif db[5] == 1:
        await dp.bot.send_photo(chat_id=db[8], photo=db[6], caption=msg1, parse_mode='HTML')
        await call.message.edit_caption(msg2, parse_mode='HTML')
    elif db[5] == 2:
        await dp.bot.send_document(chat_id=db[8], document=db[6], caption=msg1, parse_mode='HTML')
        await call.message.edit_caption(msg2, parse_mode='HTML')
    