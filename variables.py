import gspread


text_new_request = '🆕 Создать запрос'
text_my_requests = 'ℹ️ Мои запросы'
text_deferred_requests = '🔂 Полученные запросы'
text_admin_panel = '👨‍💻 Администрирование'
text_request_new = '<b>Статья расхода:</b> {}\n<b>Сумма:</b> {} руб. {}\n<b>Комментарий:</b> <i>{}</i>'
text_request_to = "<b>Запрос /N_{}</b> от <a href=\"tg://user?id={}\">{}</a>\n<b>Статья расхода:</b> {}\n\
<b>Сумма:</b> {} руб. {}\n<b>Комментарий:</b> <i>{}</i>\n<u>{}</u>"
text_request_sent = "<b>Запрос /N_{}</b>\n<b>Статья расхода:</b> {}\n<b>Сумма:</b> {} руб. {}\n<b>Комментарий:</b> <i>{}</i>\n<u>{}</u>"
text_my_stats = "\n➖ <b>Запрос /N_{}</b> <i>{}</i> <u>{} руб.</u> {}"
text_deferred_stats = "\n➖ <b>Запрос /N_{}</b> от <a href=\"tg://user?id={}\">{}</a> <i>{}</i> <u>{} руб.</u> {}"
text_deferred = "\n➖➖➖➖➖➖➖➖➖➖\n<b>Запрос №{}</b> от <a href=\"tg://user?id={}\">{}</a>\n📋<u>{}</u> 💵<b>{} руб.</b> 🕓<i>{}</i>\n📝<i>{}</i>"
text_request = "<b>Запрос /N_{}</b>"


TEXT = {'new_request': text_new_request,
        'my_requests': text_my_requests,
        'deferred_requests': text_deferred_requests,
        'admin_panel': text_admin_panel,
        'request_new': text_request_new,
        'request_to': text_request_to,
        'request_sent': text_request_sent,
        'my_stats': text_my_stats,
        'deferred_stats': text_deferred_stats,
        'deferred': text_deferred,
        'request': text_request,}


async def append_google_sheet(arr, gsheet):
    gc = gspread.service_account(filename='test-bot-329020-64a57780ff96.json')
    sh = gc.open_by_key(gsheet)
    ws = sh.sheet1
    # ws.append_row(arr, value_input_option='USER_ENTERED')
    ws.update(arr)