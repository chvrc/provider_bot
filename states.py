from aiogram.dispatcher.filters.state import State, StatesGroup

class Admin(StatesGroup):
    start = State()
    users = State()
    exp = State()
    inv_link = State()
    stats = State()
    add_user_name = State()
    add_user_id = State()
    edit_user_start = State()
    edit_user = State()
    del_user = State()
    add_exp = State()
    add_exp_user = State()
    edit_exp_start = State()
    edit_exp = State()
    del_exp = State()
    stats_cash = State()
    stats_exp = State()



class MainMenu(StatesGroup):
    new_request = State()
    my_requests = State()
    deferred_requests = State()
    admin_panel = State()


class NewRequest(StatesGroup):
    pay_method = State()
    purpose = State()
    price = State()
    comment = State()
    file = State()
    send = State()


class MyRequests(StatesGroup):
    stats = State()


class DeferredRequests(StatesGroup):
    stats = State()


class Request(StatesGroup):
    reject = State()


class NewUser(StatesGroup):
    start = State()