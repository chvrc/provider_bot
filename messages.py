

msg_start = """Привет, *%s*\!\n\
Я чат\-бот 🤖 для Заказчиков 👥\, Проектировщиков👨‍💻👩‍💻 и Поставщиков оборудования👀\. \
Укажите свой статус\.\n\
Продолжая использовать чат\-бота вы соглашаетесь с условиями Пользовательского соглашения\."""

msg_posting = """Размещение постов на канале ProektOrdering для подписчиков данного канала \
бесплатно и в неограниченном количестве\. \
Если Вы не подписаны на канал ProektOrdering\, \
то для Вас стоимость размещения одного поста составит *100* р\.\n\
__*Важно\!*__ _После оплаты перейдите обратно в бота и нажмите кнопку \"Подтвердить оплату\"\. \
Не нажимайте другие кнопки и не отправляйте сообщения до подтверждения\._"""

msg_customer = """*Уважаемый %s*\! 
Предлагаем Вам подписаться на __платный__ премиальный канал *ProektOrdering*🤘 \
или разместить пост на канале проектировщиков с заказом проекта\/раздела или рекламой\."""

msg_designer = """Предлагаем Вам \,*%s*\, подписаться на платный премиальный канал ProektOrdering🤘 \
с Заказами проектов\/разделов или разместить на данном канале пост с резюме\/вакансией\/рекламой\, \
а еще рекомендуем подписаться \(бесплатно\) на Новостной канал для проектировщиков ProektPlus ℹ️"""

msg_subscribe = """Доступ на канал ProektOrdering возможен только после оплаты\. Стоимость годовой подписки всего 499 р\.\n\
__*Важно\!*__ _После оплаты перейдите обратно в бота и нажмите кнопку \"Подтвердить оплату\"\. \
Не нажимайте другие кнопки и не отправляйте сообщения до подтверждения\._\n\
Вы готовы оплатить подписку\?"""

msg_supplier = """Предлагаем Вам\, *%s*\, подписаться на платный премиальный канал ProektOrdering🤘 \
с возможностью размещения на данном канале рекламы Вашего оборудования\, так же возможно разовое \
размещения рекламы оборудования на канале\, а еще рекомендуем подписаться \(бесплатно\) на \
Новостной канал для проектировщиков ProektPlus ℹ️"""

msg_sub_benefits = """Платная подписка на канал *ProektOrdering* обеспечивает Вам\:
\- бесплатный доступ ко всем постам, которые размещают на данном канале Заказчики\/Проектировщики\/Поставщики\; 
\- бесплатное размещение постов на данном канале через \@ProektOrderingBot в неограниченном количестве\.
 Подписка оформляется на год с последующей возможностью ее продления\."""

msg_sub_posting = """Вы выбрали создание поста на канале «ProektOrdering»\. 
Отправьте боту то\, что хотите опубликовать\: заявку на выполнение проекта\, \
раздела\, реклама\, поиск сотрудника в штат и т\.д\."""

msg_post_file = """Для привлечения внимания дополните пост изображением\, \
видео, GIF, документом или нажмите кнопку \"Пропустить\"\."""

MESSAGES = {'start': msg_start,
            'posting': msg_posting,
            'customer': msg_customer,
            'designer': msg_designer,
            'subscribe': msg_subscribe,
            'supplier': msg_supplier,
            'sub_benefits': msg_sub_benefits,
            'sub_posting': msg_sub_posting,
            'post_file': msg_post_file,}