from aiogram.dispatcher.storage import FSMContext
from aiogram.types.input_media import InputMediaAnimation
from aiogram.dispatcher.filters import Command
from aiogram.types.message import ContentType
from aiogram.types.reply_keyboard import ReplyKeyboardRemove
from database import add_request, add_user, del_user, edit_request, get_deferred_requests, get_exp, get_exps, get_request, get_requests, get_user, get_users
from keyboards import kb_admin, kb_back, kb_confirm, kb_edit, kb_exp, kb_pay_method, kb_request, kb_request_cashless, kb_send, kb_skip, kb_start, kb_users
from load_bot import dp, bot
from aiogram import types
from states import NewRequest

from variables import TEXT


@dp.callback_query_handler(lambda c: c.data == 'back', state=NewRequest.purpose)
async def new_request_start(call: types.CallbackQuery, state: FSMContext):
    db = await get_user(call.from_user.id)
    if db:
        async with state.proxy() as data:
            data['from_uid'] = db[0][1]
            data['from_name'] = db[0][2]
        kb = await kb_pay_method()
        await call.message.edit_text('Выберите способ оплаты', reply_markup=kb)
        await NewRequest.pay_method.set()
    else:
        await state.finish()
        await call.message.edit_text('Вас нет в базе пользователей. Обратитесь к администратору.')


@dp.callback_query_handler(state=NewRequest.pay_method)
async def new_request_back(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:
        if call.data == 'cash':
            data['pay_method'] = 'Наличные'
            data['pay_icon'] = '💵'
        else:
            data['pay_method'] = 'Безналичные'
            data['pay_icon'] = '💳'
    kb = await kb_exp()
    await call.message.edit_text('Выберите статью расхода', reply_markup=kb)
    await NewRequest.purpose.set()


@dp.callback_query_handler(lambda c: c.data.startswith('exp'), state=NewRequest.purpose)
async def request_exp_choice(call: types.CallbackQuery, state: FSMContext):
    db = await get_exp(call.data[3:])
    async with state.proxy() as data:
        data['to_uid'] = db[0]
        data['to_name'] = db[1]
        data['purpose'] = db[2]
    await call.message.edit_text('Введите необходимую сумму в рублях (целое число)')
    await NewRequest.price.set()


@dp.message_handler(lambda message: message.text.isdigit() ,state=NewRequest.price)
async def request_price(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['price'] = message.text
    await message.answer('Введите комментарий')
    await NewRequest.comment.set()


@dp.message_handler(state=NewRequest.price, content_types=ContentType.ANY)
async def request_price(message: types.Message, state: FSMContext):
    await message.answer('Ошибка! Введите целое число!')


@dp.message_handler(state=NewRequest.comment)
async def request_comment(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['comment'] = message.text
    kb = await kb_skip()
    await message.answer('Загрузите фото или файл с расчетами', reply_markup=kb)
    await NewRequest.file.set()


@dp.callback_query_handler(lambda c: c.data == 'skip', state=NewRequest.file)
async def request_no_photo(call: types.CallbackQuery, state: FSMContext):
    kb = await kb_send()
    async with state.proxy() as data:
        data['file_type'] = 0
        data['file'] = None
        data['file_url'] = None
        ans = TEXT['request_new'].format(
            data['purpose'], data['price'], data['pay_icon'], data['comment']
            )
        await call.message.edit_text(text=ans,
                reply_markup=kb, parse_mode='HTML')
    await NewRequest.send.set()


@dp.message_handler(state=NewRequest.file, content_types=ContentType.PHOTO)
async def request_photo(message: types.Message, state: FSMContext):
    kb = await kb_send()
    async with state.proxy() as data:
        data['file_type'] = 1
        data['file'] = message.photo[-1].file_id
        data['file_url'] = await message.photo[-1].get_url()
        ans = TEXT['request_new'].format(
            data['purpose'], data['price'], data['pay_icon'], data['comment']
            )
        await message.answer_photo(photo=data['file'],
                caption=ans,
                reply_markup=kb, parse_mode='HTML')
    await NewRequest.send.set()


@dp.message_handler(state=NewRequest.file, content_types=ContentType.DOCUMENT)
async def request_photo(message: types.Message, state: FSMContext):
    kb = await kb_send()
    async with state.proxy() as data:
        data['file_type'] = 2
        data['file'] = message.document.file_id
        data['file_url'] = await message.document.get_url()
        ans = TEXT['request_new'].format(
            data['purpose'], data['price'], data['pay_icon'], data['comment']
            )
        await message.answer_document(document=data['file'],
                caption=ans,
                reply_markup=kb, parse_mode='HTML')
    await NewRequest.send.set()


@dp.callback_query_handler(lambda c: c.data == 'send', state=NewRequest.send)
async def send_request(call: types.CallbackQuery, state: FSMContext):
    async with state.proxy() as data:

        request = await add_request(data['pay_method'], data['purpose'], data['price'], data['comment'], data['file_type'],
                                    data['file'], data['from_uid'], data['from_name'], data['to_uid'], data['to_name'], 0, 'На рассмотрении',  data['file_url'])
        if data['pay_method'] == 'Наличные':
            kb = await kb_request(request[0])
        else:
            kb = await kb_request_cashless(request[0])

        msg1 = TEXT['request_to'].format(request[0], data['from_uid'], data['from_name'],
                            data['purpose'], data['price'], data['pay_icon'], data['pay_method'], data['comment'], '❕Новый запрос')
        msg2 = TEXT['request_sent'].format(request[0], data['purpose'],
                                        data['price'], data['pay_icon'], data['comment'], '❕Запрос отправлен')

        if data['file_type'] == 0:
            await dp.bot.send_message(chat_id=data['to_uid'],
                    text=msg1,
                    reply_markup=kb, parse_mode='HTML')
            await call.message.edit_text(msg2, parse_mode='HTML')
        elif data['file_type'] == 1:
            await dp.bot.send_photo(chat_id=data['to_uid'], photo=data['file'],
                    caption=msg1,
                    reply_markup=kb, parse_mode='HTML')
            await call.message.edit_caption(msg2,
                    parse_mode='HTML')
        elif data['file_type'] == 2:
            await dp.bot.send_document(chat_id=data['to_uid'], document=data['file'],
                    caption=msg1,
                    reply_markup=kb, parse_mode='HTML')
            await call.message.edit_caption(msg2,
                    parse_mode='HTML')
    await state.finish()


@dp.callback_query_handler(lambda c: c.data == 'back', state=NewRequest.send)
async def send_request(call: types.CallbackQuery, state: FSMContext):
    await call.message.edit_text('Запрос отменён')
    await state.finish()