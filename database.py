import sqlite3
import time
from datetime import datetime


async def create_db():
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""PRAGMA foreign_keys=ON;
                    """)
    cur.execute("""CREATE TABLE IF NOT EXISTS users(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    uid INTEGER UNIQUE,
                    name TEXT)
                    """)
    cur.execute("""CREATE TABLE IF NOT EXISTS expense_items(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    name TEXT,
                    user INTEGER,
                    FOREIGN KEY(user) REFERENCES users(id) ON DELETE CASCADE)
                    """)
    cur.execute("""CREATE TABLE IF NOT EXISTS requests(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    pay_method TEXT,
                    purpose TEXT,
                    price INT,
                    comment TEXT,
                    file_type INTEGER,
                    file TEXT,
                    ask_time INTEGER,
                    from_uid INTEGER,
                    from_name TEXT,
                    to_uid INTEGER,
                    to_name TEXT,
                    status INTEGER,
                    status_name TEXT,
                    status_time INTEGER,
                    reject_reason TEXT,
                    file_url TEXT
                    )
                    """)
    conn.commit()


async def add_user(uid, name):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE INTO users(uid, name) VALUES(?, ?)
                """, (uid, name))
    conn.commit()
    return uid, name


async def edit_user(id, name):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                UPDATE users
                SET name = ?
                WHERE id = ?
                """, (name, id))
    conn.commit()


async def get_user(uid):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM users
                WHERE uid = ?
                """, (uid, ))
    res = cur.fetchall()
    conn.commit()
    return res


def check_user(uid):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT EXISTS(
                SELECT 1 FROM users
                WHERE uid = ?)
                """, (uid, ))
    res = cur.fetchall()
    conn.commit()
    if res[0][0] == 1:
        return True
    else:
        return False


async def get_users():
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM users
                """)
    res = cur.fetchall()
    conn.commit()
    return res


async def del_user(id):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""PRAGMA foreign_keys=ON;
                    """)
    cur.execute("""
                SELECT * FROM users
                WHERE id = ?
                """, (id, ))
    res = cur.fetchall()
    cur.execute("""
                DELETE from users
                WHERE id=?;
                """, (id, ))
    conn.commit()
    return res[0]


async def add_exp(name, user):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE INTO expense_items(name, user) VALUES(?, ?)
                """, (name, user))
    cur.execute("""
                SELECT u.uid, u.name, e.name
                FROM expense_items e
                INNER JOIN users u ON e.user = u.id
                ORDER BY e.id DESC
                LIMIT 1
                """)
    res = cur.fetchall()
    conn.commit()
    return res[0]


async def edit_exp_name(id, name):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                UPDATE expense_items
                SET name = ?
                WHERE id = ?
                """, (name, id))
    cur.execute("""
                SELECT u.uid, u.name, e.name
                FROM expense_items e
                INNER JOIN users u ON e.user = u.id
                WHERE e.id = ?
                """, (id, ))
    res = cur.fetchall()
    conn.commit()
    return res[0]


async def edit_exp_user(id, user):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                UPDATE expense_items
                SET user = ?
                WHERE id = ?
                """, (user, id))
    cur.execute("""
                SELECT u.uid, u.name, e.name
                FROM expense_items e
                INNER JOIN users u ON e.user = u.id
                WHERE e.id = ?
                """, (id, ))
    res = cur.fetchall()
    conn.commit()
    return res[0]


async def get_exp(id):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT u.uid, u.name, e.name
                FROM expense_items e
                INNER JOIN users u ON e.user = u.id
                WHERE e.id = ?
                """, (id, ))
    res = cur.fetchall()
    conn.commit()
    return res[0]


async def get_exp_name(id):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT name
                FROM expense_items
                WHERE id = ?
                """, (id, ))
    res = cur.fetchall()
    conn.commit()
    return res[0][0]


async def get_exps():
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT e.id, e.name, u.uid, u.name
                FROM expense_items e
                INNER JOIN users u ON e.user = u.id
                """)
    res = cur.fetchall()
    conn.commit()
    return res


def get_exp_user():
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT u.uid
                FROM expense_items e
                INNER JOIN users u ON e.user = u.id
                """)
    res = cur.fetchall()
    conn.commit()
    ans = []
    for i in res:
        ans.append(i[0])
    return ans


async def del_exp(id):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                DELETE from expense_items
                WHERE id=?;
                """, (id, ))
    conn.commit()


async def add_request(pay_method, purpose, price, comment, file_type, file, from_uid, from_name, to_uid, to_name, status, status_name, file_url):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                INSERT OR IGNORE INTO requests(pay_method, purpose, price, comment, file_type, file, ask_time, from_uid, from_name, to_uid, to_name, status, status_name, status_time, file_url)
                VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                """, (pay_method, purpose, price, comment, file_type, file, int(time.time()), from_uid, from_name, to_uid, to_name, status, status_name, int(time.time()), file_url))
    cur.execute("""
                SELECT *
                FROM requests
                ORDER BY id DESC
                LIMIT 1
                """)
    res = cur.fetchall()
    conn.commit()
    return res[-1]


async def edit_request(id, status, status_name, reject_reason=None):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                UPDATE requests
                SET status = ?, status_name = ?, status_time = ?, reject_reason = ?
                WHERE id = ?
                """, (status, status_name, int(time.time()), reject_reason, id))
    cur.execute("""
                SELECT * FROM requests
                WHERE id = ?
                """, (id, ))
    res = cur.fetchall()
    conn.commit()
    return res[0]


async def get_request(id):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM requests
                WHERE id = ?
                """, (id, ))
    res = cur.fetchall()
    conn.commit()
    return res[0]


async def get_request_by_id(uid, req_id):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM requests
                WHERE (from_uid = ? OR to_uid = ?) AND id = ?
                """, (uid, uid, req_id))
    res = cur.fetchone()
    conn.commit()
    return res


async def get_requests(uid):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM requests
                WHERE from_uid = ?
                """, (uid, ))
    res = cur.fetchall()
    conn.commit()
    return res


async def get_requests_by_status(uid, status):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM requests
                WHERE from_uid = ? AND status = ?
                """, (uid, status))
    res = cur.fetchall()
    conn.commit()
    return res


async def get_requests_by_status_other(uid, pay_method):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM requests
                WHERE from_uid = ? AND pay_method = ? AND status IN (0, 2, 5)
                """, (uid, pay_method))
    res = cur.fetchall()
    conn.commit()
    return res


async def get_deferred_requests(uid, status):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM requests
                WHERE to_uid = ? AND status = ?
                """, (uid, status))
    res = cur.fetchall()
    conn.commit()
    return res


async def pay_all_deferred_requests(uid):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT * FROM requests
                WHERE to_uid = ? AND status = 0
                """, (uid, ))
    res = cur.fetchall()
    conn.commit()
    return res


def check_deferred_requests(uid):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT EXISTS(
                SELECT 1 FROM requests
                WHERE to_uid = ?)
                """, (uid, ))
    res = cur.fetchall()
    conn.commit()
    if res[0][0] == 1:
        return True
    else:
        return False


def check_my_requests(uid):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT EXISTS(
                SELECT 1 FROM requests
                WHERE from_uid = ?)
                """, (uid, ))
    res = cur.fetchall()
    conn.commit()
    if res[0][0] == 1:
        return True
    else:
        return False



async def get_all_requests():
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT id, pay_method, purpose, price, comment, ask_time, from_name, from_uid, to_name, to_uid, status_name, status_time, reject_reason, file_url
                FROM requests
                """)
    res = cur.fetchall()
    conn.commit()
    arr = [('№ запроса', 'Метод оплаты', 'Статья', 'Сумма', 'Комментарий', 'Время запроса', 'Запросивший',
            'ID запросившего', 'Ответственный', 'ID ответственного', 'Статус', 'Время смены статуса', 'Причина отказа', 'Прикрепления', )]
    for i in res:
            arr.append((i[0], i[1], i[2], i[3], i[4], str(datetime.fromtimestamp(i[5])), i[6], i[7], i[8], i[9], i[10], str(datetime.fromtimestamp(i[11])), i[12], i[13]))
    return arr


async def get_all_requests_cash(method, ttime):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT id, pay_method, purpose, price, comment, ask_time, from_name, from_uid, to_name, to_uid, status_name, status_time, reject_reason
                FROM requests
                WHERE pay_method = ? AND ask_time > ?
                """, (method, int(time.time() - ttime * 60 * 60 * 24)))
    res = cur.fetchall()
    conn.commit()
    arr = [('№ запроса', 'Метод оплаты', 'Статья', 'Сумма', 'Комментарий', 'Время запроса', 'Запросивший',
            'ID запросившего', 'Ответственный', 'ID ответственного', 'Статус', 'Время смены статуса', 'Причина отказа', )]
    for i in res:
        if i[12] != None:
            arr.append((i[0], i[1], i[2], i[3], i[4], str(datetime.fromtimestamp(i[5])), i[6], i[7], i[8], i[9], i[10], str(datetime.fromtimestamp(i[11])), i[12]))
        else:
            arr.append((i[0], i[1], i[2], i[3], i[4], str(datetime.fromtimestamp(i[5])), i[6], i[7], i[8], i[9], i[10], str(datetime.fromtimestamp(i[11])), '-'))
    return arr


async def get_all_requests_exp(method, ttime):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT name
                FROM expense_items
                WHERE id = ?
                """, (method, ))
    purpose = cur.fetchall()
    cur.execute("""
                SELECT id, pay_method, purpose, price, comment, ask_time, from_name, from_uid, to_name, to_uid, status_name, status_time, reject_reason
                FROM requests
                WHERE purpose = ? AND ask_time > ?
                """, (purpose[0][0], int(time.time() - ttime * 60 * 60 * 24)))
    res = cur.fetchall()
    conn.commit()
    arr = [('№ запроса', 'Метод оплаты', 'Статья', 'Сумма', 'Комментарий', 'Время запроса', 'Запросивший',
            'ID запросившего', 'Ответственный', 'ID ответственного', 'Статус', 'Время смены статуса', 'Причина отказа', )]
    for i in res:
        if i[12] != None:
            arr.append((i[0], i[1], i[2], i[3], i[4], str(datetime.fromtimestamp(i[5])), i[6], i[7], i[8], i[9], i[10], str(datetime.fromtimestamp(i[11])), i[12]))
        else:
            arr.append((i[0], i[1], i[2], i[3], i[4], str(datetime.fromtimestamp(i[5])), i[6], i[7], i[8], i[9], i[10], str(datetime.fromtimestamp(i[11])), '-'))
    return arr


async def my_request_stats(uid):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    res = []
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE from_uid = ? AND status = 0
                """, (uid, ))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE from_uid = ? AND status = 2
                """, (uid, ))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE from_uid = ? AND status = 3
                """, (uid, ))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE from_uid = ?
                """, (uid, ))
    res.append(cur.fetchone())
    conn.commit()
    return res


async def deferred_request_stats(uid):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    res = []
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE to_uid = ? AND status = 1
                """, (uid, ))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE to_uid = ? AND status = 2
                """, (uid, ))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE to_uid = ? AND status = 3
                """, (uid, ))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE to_uid = ?
                """, (uid, ))
    res.append(cur.fetchone())
    conn.commit()
    return res


async def all_stats():
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    res = []
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                """)
    res.append(cur.fetchall()[0])
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE status = 2
                """)
    res.append(cur.fetchall()[0])
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE status = 2 AND pay_method = 'Наличные'
                """)
    res.append(cur.fetchall()[0])
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE status = 2 AND pay_method = 'Безналичные'
                """)
    res.append(cur.fetchall()[0])
    conn.commit()
    return res


async def all_stats_exp():
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    res = []
    cur.execute("""
                SELECT name FROM expense_items
                """)
    exps = cur.fetchall()
    for i in exps:
        if i:
            cur.execute("""
                        SELECT Count(*), SUM(price) FROM requests
                        WHERE status = 2 AND purpose = ?
                        """, i)
            res.append(i + cur.fetchall()[0])
    return res


async def request_stats_pay(pay_method, ttime):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    res = []
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE pay_method = ? AND status = 0 AND ask_time > ?
                """, (pay_method, int(time.time() - 60 * 60 * 24 * ttime)))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE pay_method = ? AND status = 1 AND ask_time > ?
                """, (pay_method, int(time.time() - 60 * 60 * 24 * ttime)))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE pay_method = ? AND status = 2 AND ask_time > ?
                """, (pay_method, int(time.time() - 60 * 60 * 24 * ttime)))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE pay_method = ? AND status = 3 AND ask_time > ?
                """, (pay_method, int(time.time() - 60 * 60 * 24 * ttime)))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE pay_method = ? AND ask_time > ?
                """, (pay_method, int(time.time() - 60 * 60 * 24 * ttime)))
    res.append(cur.fetchone())
    conn.commit()
    return res


async def request_stats_exp(purpose, ttime):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    res = []
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE purpose = ? AND status = 0 AND ask_time > ?
                """, (purpose, int(time.time() - 60 * 60 * 24 * ttime)))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE purpose = ? AND status = 1 AND ask_time > ?
                """, (purpose, int(time.time() - 60 * 60 * 24 * ttime)))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE purpose = ? AND status = 2 AND ask_time > ?
                """, (purpose, int(time.time() - 60 * 60 * 24 * ttime)))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE purpose = ? AND status = 3 AND ask_time > ?
                """, (purpose, int(time.time() - 60 * 60 * 24 * ttime)))
    res.append(cur.fetchone())
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE purpose = ? AND ask_time > ?
                """, (purpose, int(time.time() - 60 * 60 * 24 * ttime)))
    res.append(cur.fetchone())
    conn.commit()
    return res


async def check_request_0(uid):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE to_uid = ? AND status = 0
                """, (uid, ))
    res = cur.fetchone()
    conn.commit()
    return res


async def check_request_1(uid):
    conn = sqlite3.connect('appdb.db')
    cur = conn.cursor()
    cur.execute("""
                SELECT Count(*), SUM(price) FROM requests
                WHERE from_uid = ? AND status = 1
                """, (uid, ))
    res = cur.fetchone()
    conn.commit()
    return res